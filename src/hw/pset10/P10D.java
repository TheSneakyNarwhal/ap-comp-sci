package hw.pset10;

public class P10D {

    public static void main(String[] args) {

        int[] nums = {3, 7, 4, 6, 4, 1, 5, 8, 4, 5, 2};


        // Make two parallel arrays: values[] and frequencies[].

        // values[] will hold each unique number from the nums[] array.

        // frequencies[] will hold the number of times the corresponding value

        // appears in nums[]



        int[] values = new int[nums.length];

        int[] frequencies = new int[nums.length];

        // The values[] array is not full. numValues will hold its logical size...

        int numValues = findValuesAndFrequencies(nums, values, frequencies);



        // Print a table of the values and frequencies
        System.out.println("Values\tFrequency");
        for (int i = 0; i < numValues; i++) {
            System.out.println(values[i] + "\t\t" + frequencies[i]);
        }

    }

    private static int findValuesAndFrequencies(int[] nums, int[] values, int[] frequencies) {
        int count = 0;

        outer:
        for (int num : nums) {
            for (int j = 0; j < count; j++) {
                if (values[j] == num) {
                    frequencies[j]++;
                    continue outer;
                }
            }
            values[count] = num;
            frequencies[count] = 1;
            count++;
        }

        return count;
    }


}