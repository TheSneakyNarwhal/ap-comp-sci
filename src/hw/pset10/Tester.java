package hw.pset10;

import TurtleGraphics.StandardPen;

public class Tester {
    public static void main(String[] args) {
        StandardPen pen = new StandardPen();

        Circle circle = new Circle(0, 0, 25);
        circle.draw(pen);

        Rect rect = new Rect(0, 0, 50, 50);
        rect.draw(pen);
    }
}
