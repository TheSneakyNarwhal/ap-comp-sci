package hw.pset10;

public class ArrayBasics {

    public static void main(String[] args) {
        int[] nums = new int[5];

        nums[0] = 3;
        nums[1] = 5;

        System.out.println(nums[0] + nums[1]);

        double[] testScores = new double[3];
        testScores[0] = 65.8;
        testScores[1] = 84;
        testScores[2] = 99.6;

        int[] lottoNumbers = {30895, 32764, 39726, 36000};

        for (int i = 0; i < lottoNumbers.length; i++) {
            System.out.println(lottoNumbers[i]);
        }

        int sum = 0;
        for (double score : testScores) {
            sum += score;
        }

        double average = sum / testScores.length;
        System.out.println("Average score: " + average);
    }
}
