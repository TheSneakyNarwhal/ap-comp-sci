package hw.pset10;

import processing.core.PApplet;
public class Ball
{
    private static int[][] COLORS = {{255, 0, 0}, { 255, 127, 0}, { 255, 255, 0}, {0, 255, 0}, {0, 0, 255}, { 75, 0, 130}};
    private PApplet applet;
    private int size;
    private float x, y, xSpeed, ySpeed;   // Processing uses the float data type instead of doubles
    private int colorIndex;

    public Ball(PApplet applet_, float x_, float y_, float xSpeed_, float ySpeed_, int colorIndex_)
    {
        applet = applet_;
        x = x_;
        y = y_;
        xSpeed = xSpeed_;
        ySpeed = ySpeed_;
        size = 20;
        colorIndex = colorIndex_  % Ball.COLORS.length;
    }

    public void display()
    {
        int[] color = COLORS[colorIndex];
        applet.noStroke();
        applet.fill(color[0], color[1], color[2]);
        applet.ellipseMode(PApplet.CENTER);
        applet.ellipse(x, y, size, size);
    }

    public void move()
    {
        x += xSpeed;
        y += ySpeed;

        boolean hitWall = false;

        if (x > applet.width || x < 0)
        {
            xSpeed *= -1;   // reverse the x direction of the ball
            hitWall = true;
        }

        if (y > applet.height || y < 0)
        {
            ySpeed *= -1;   // reverse the y direction of the ball
            hitWall = true;
        }

        if (hitWall) {
            colorIndex++;
            colorIndex %= COLORS.length;
        }
    }
}