package hw.pset10.Crypto;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

public class Crypto {
    private static char[] ALPHABET = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

    public static void main(String[] args) throws IOException {
        String string = filetoString("derp.txt");

        int[] charCount = countChars(string);

        System.out.println("Letter \tCount");
        for (int i = 0; i < ALPHABET.length; i++) {
            System.out.println(ALPHABET[i] + "\t\t" + charCount[i]);
        }

        System.out.println("Letter \tFrequency");
        float[] freq = charFreq(string);
        for (int i = 0; i < ALPHABET.length; i++) {
            System.out.printf(ALPHABET[i] + "\t\t%.2f\n", freq[i]);
        }

        int randomShift = new Random().nextInt(100) - 50;
        System.out.println("Shift:\t\t\t\t" + randomShift);

        String encrypted = caesarCipher(string, randomShift);
        System.out.println("Encrypted:\t\t\t" + encrypted);

        String decrypted = caesarCipher(encrypted, -randomShift);
        System.out.println("Decrypt:\t\t\t" + decrypted);

        String attemptDecrypt = attemptDecryption(encrypted);
        System.out.println("Decrypt attempt:\t" + attemptDecrypt);

        File file = new File("encrypted.txt");
        PrintWriter printWriter = new PrintWriter(file);
        printWriter.println(encrypted);
        printWriter.close();
    }

    // counts chars a-z to an array
    public static int[] countChars(String string) {
        string = string.toLowerCase();
        int[] charCount = new int[ALPHABET.length];

        for (char c : string.toCharArray()) {
            if (c >= 'a' && c <= 'z') charCount[c - 'a']++;
        }
        return charCount;
    }

    // uses values from countChars to find the percent frequency
    public static float[] charFreq(String string) {
        int[] charCount = countChars(string);
        float[] freq = new float[ALPHABET.length];

        for (int i = 0; i < ALPHABET.length; i++) {
            freq[i] = (float) charCount[i] / string.length();
        }

        return freq;
    }

    public static String caesarCipher(String string, int shift) {
        string = string.toLowerCase();

        StringBuilder stringBuilder = new StringBuilder();
        for (int num : string.toCharArray()) {
            if (num >= 'a' && num <= 'z') {
                // shift int so that 'a' = 0 and 'z' = 25 instead of 'a' = 97 and 'z' = 122
                num = (num + shift - 'a') % ALPHABET.length;

                if (num < 0) num += ALPHABET.length;

                // shift back
                num += 'a';
            }

            stringBuilder.append((char) num);
        }
        return stringBuilder.toString();
    }

    public static String attemptDecryption(String string) {
        int[] charCount = countChars(string);

        int maxIndex = 0;

        // finds char with highest frequency
        for (int i = 0; i < charCount.length; i++) {
            if (charCount[i] > charCount[maxIndex]) maxIndex = i;
        }

        // shifts letter to the place of 'e'; index 4 or ('e' - 'a')
        System.out.println("Shift by: " + (4 - maxIndex));
        return caesarCipher(string, 4 - maxIndex);
    }

    public static String attemptDecryption2(String string) throws IOException {
        String[] words = string.split(" ");

        for (int i = 0; i < words.length; i++) {
            String word = words[i];

            // prevents ambiguity between words less than 4 letters such as "the" and "max"
            if (word.length() < 4 && i < words.length - 1) continue;

            for (int j = 0; j < ALPHABET.length; j++) {
                // remove non alphabetic characters to check for a dictionary match
                String shiftedWord = caesarCipher(word, j).replaceAll("[^a-zA-Z]", "").toLowerCase();

                // load dictionary file of the starting letter
                Scanner scanner = new Scanner(new File(Character.toUpperCase(shiftedWord.charAt(0)) + " Words.txt"));

                while (scanner.hasNext()) {
                    // if word matched then return the entire string decrypted
                    if (scanner.next().equals(shiftedWord)) return caesarCipher(string, j);
                }
            }
        }

        return "ERROR: SHIFT NOT FOUND!";
    }

    // concat all words to a single string
    public static String filetoString(String path) throws IOException {
        Scanner scanner = new Scanner(new File(path));

        StringBuilder stringBuilder = new StringBuilder();

        while (scanner.hasNext()) {
            stringBuilder.append(scanner.next());
            stringBuilder.append(' ');
        }

        return stringBuilder.toString();
    }
}
