package hw.pset10.Crypto;

import java.util.Scanner;
import java.io.IOException;
import java.io.File;

public class Alex
{
    public static void main(String[] args) throws IOException
    {
        decode("encrypted.txt");
    }

    public static void decode(String filename) throws IOException
    {
        Scanner reader = new Scanner(new File(filename));

        String str = "";

        while (reader.hasNext()) //read encrypted text file
        {
            str += reader.next();
            str += " ";
        }
        str = str.toLowerCase();

        String encrypted = str;
        String[] fileArray = (removePunct(str)).split("");
        double[] freqs = new double[26];
        String[] values = new String[26];

        countLetters(fileArray, freqs, values);
        getLetterFreqs(freqs, encrypted);

        int valueIndex = mostCommonChar(freqs);

        //find number of shift
        char fakeE = fileArray[valueIndex].charAt(0);
        int shift = (int)fakeE - 'e';
        System.out.println("shift: "+shift);
        String decrypted = "";

        for (int i = 0; i < encrypted.length(); i++) // lastly, decrypt file by shifting all a-z characters by x amount
        {
            int letter = encrypted.charAt(i);
            if (letter < ' ' || letter > '/')
            {
                letter -= shift;
            }
            if (letter < 'a' && letter + shift >= 'a')
            // if decrypted letter comes before 'a', shift 26 right
            {
                letter += 26;
            }
            if (letter > 'z' && letter + shift <= 'z')
            // if decrypted letter comes after 'z', shift 26 left
            {
                letter -= 26;
            }
            if (letter + shift > '/' && letter + shift < '`')
            // if decrypted letter is not part of the alphabet, shift back to original
            {
                letter += shift;
            }
            decrypted += (char)letter;

        }
        System.out.print(decrypted);
    }

    public static int mostCommonChar (double[] freqs)
    {
        int valueIndex = 0;
        for (int i = 0; i < freqs.length; i++)
        {
            if (freqs[i] > .11) //frequency of letter E = 0.127, no other characters have a frequency greater than .1
            {
                valueIndex = i;
                break;
            }
        }
        return valueIndex;
    }

    public static void countLetters (String[] fileArray, double[] freqs, String[] values)
    {
        int arrLength = 0;
        for (int i = 0; i < fileArray.length; i++) //create unique character array + character counts
        {
            boolean unique = true;
            for (int j = 0; j < arrLength; j++)
            {
                if (values[j].equals(fileArray[i]))
                {
                    unique = false;
                    freqs[j]++;
                    break;
                }
            }
            if (unique == true && arrLength < 26 )
            {
                values[arrLength] = fileArray[i];
                arrLength++;
            }
        }
    }

    public static void getLetterFreqs (double[] freqs, String encrypted)
    {
        for (int i = 0; i < freqs.length; i++) //change counts to relative freqs
        {
            freqs[i] /= encrypted.length();
        }
    }

    public static String removePunct(String str)
    {
        String noPunct = "";
        for (int i = 0; i < str.length(); i++)
        {
            int letter = str.charAt(i);
            if (letter > 'a' && letter < 'z')
            {
                noPunct += (char)letter;
            }

        }
        return noPunct;
    }
}