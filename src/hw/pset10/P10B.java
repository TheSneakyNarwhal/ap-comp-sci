package hw.pset10;

import java.io.*;
import java.util.Scanner;

public class P10B {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("derp.txt"));
        String[] words = new String[100];

        int count = 0;
        while (scanner.hasNext()) {
            words[count] = scanner.next();
            count++;
        }

        for (int i = 0; i < count; i++) {
            System.out.println(words[i]);
        }
    }
}
