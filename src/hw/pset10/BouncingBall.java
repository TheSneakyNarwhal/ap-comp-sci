package hw.pset10;

import processing.core.PApplet;

import java.util.ArrayList;
import java.util.Random;

public class BouncingBall extends PApplet
{
    private ArrayList<Ball> balls;

    public static void main(String[] args)
    {
        PApplet.main(new String[] {"hw.pset10.BouncingBall"});
    }

    public void setup()
    {
        size(600, 600);

        balls = new ArrayList<>();

        for (int i = 0; i < 5000; i++) {
            addBall();
        }
    }

    public void keyPressed() {
        if (key == 'b') {
            addBall();
        }
        System.out.println(balls.size());
    }

    public void addBall() {
        Random rand = new Random();
        int xSpeed = rand.nextBoolean() ? -2 : 2;
        int ySpeed = rand.nextBoolean() ? -2 : 2;
        balls.add(new Ball(this, rand.nextInt(this.width), rand.nextInt(this.height), xSpeed, ySpeed, rand.nextInt(6)));
    }

    public void draw()
    {
        background(0);
        // By encapsulating all of the Ball's information and behaviors into a separate class,
        // we can write very simple and easy-to-understand code.

        for (Ball b : balls) {
            b.move();
            b.display();
        }
    }
}
