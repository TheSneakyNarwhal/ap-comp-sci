package hw.pset10;

public class PrimitiveTest {
    public static void main(String[] args) {
        String string = "derp";
        changeString(string);
        System.out.println(string);

        int number = 0;
        loop(number);
        System.out.println(number);

        Derp derp = new Derp("jeff");
        changeObject(derp);
        System.out.println(derp.name);
    }

    public static int loop(int val) {
        for (int i = 0; i < 100; i++) {
            val += i;
        }
        return val;
    }

    public static void changeString(String string) {
        string = "herp";
    }

    public static void changeObject(Derp derp) {
        derp.name = "new name";
    }
}

class Derp {
    String name;

    Derp(String name_) {
        name = name_;

        boolean x = false;
        boolean y = false;
        if ((x && y) || !(x && y)) {

        }
    }
}
