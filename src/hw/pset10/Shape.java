package hw.pset10;

import TurtleGraphics.Pen;

public interface Shape {
    double area();
    void draw(Pen p);
    double getXPos();
    double getYPos();
    void move(double xLoc, double yLoc);
    void stretchBy(double factor);
    String toString();
}
