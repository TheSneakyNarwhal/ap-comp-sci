package hw.pset10.Golf;

import java.util.Scanner;

public class GolfDriver
{
    public static void main(String[] args)
    {
        // Create golf data object
        GolfData round1 = new GolfData();

        // Loop to enter golf scores (-1 after last score)
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter scores (-1 when done): ");
        int score = 0;
        while (score >= 0)
        {
            score = reader.nextInt();
            if (score == -1)
                break;
            else
                round1.addScore(score);
        }
        round1.printScores();
        
        // getTotal
        System.out.println("Total score: " + round1.getTotal());

        // Look for hole-in-one; return "true" if there was one, "false" otherwise
        if (round1.holeInOne()) {
            System.out.println("You got a hole in one!");
        }
        
    }
}
