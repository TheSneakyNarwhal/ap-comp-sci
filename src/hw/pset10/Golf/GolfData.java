package hw.pset10.Golf;

public class GolfData {
    int[] scores;
    int numScores;  // keeps track of # of scores entered into array

    // Constructor
    public GolfData() {
        scores = new int[18];
        numScores = 0;
    }

    public void printScores() {
        for (int i = 0; i < numScores; i++) {
            System.out.print(scores[i] + " ");
        }
        System.out.println();
    }

    public void addScore(int scr) {
        scores[numScores] = scr;
        numScores++;
    }

    public int getTotal() {
        int sum = 0;
        for (int i = 0; i < numScores; i++) {
            sum += scores[i];
        }
        return sum;
    }

    public boolean holeInOne() {
        for (int i = 0; i < numScores; i++) {
            if (scores[i] == 1) return true;
        }
        return false;
    }
}
