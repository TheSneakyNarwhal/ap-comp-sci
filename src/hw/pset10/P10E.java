package hw.pset10;

import processing.core.PApplet;

public class P10E extends PApplet {
    public static final int WIDTH = 500;
    public static final int HEIGHT = 500;

    private Snowflake[] snowflakes;
    private static final int SNOWFLAKE_COUNT = 150;

    public static void main(String[] args) {
        PApplet.main(new String[]{"hw.pset10.P10E"});
    }

    public void setup() {
        size(WIDTH, HEIGHT);
        noStroke();

        snowflakes = new Snowflake[SNOWFLAKE_COUNT];

        for (int i = 0; i < SNOWFLAKE_COUNT; i++) {
            snowflakes[i] = new Snowflake(this);
        }
    }

    public void draw() {
        background(0);

        for (Snowflake snowflake : snowflakes) {
            snowflake.move();
            snowflake.draw();
        }

    }
}
