package hw.pset10;

import processing.core.PApplet;

import java.util.ArrayList;

public class Planet extends PApplet {
    public static final int WIDTH = 500;
    public static final int HEIGHT = 500;


    public static void main(String[] args) {
        PApplet.main(new String[]{"hw.pset10.Planet"});
    }

    public void setup() {
        size(WIDTH, HEIGHT);

        background(0);

        ArrayList<Float> pointsX = new ArrayList<>();
        ArrayList<Float> pointsY = new ArrayList<>();

        for (float i = -1f; i < 1f; i += .01f) {
            float y = sqrt(1 - i * i) * 100;
            float x = i * 100;


            if (pointsX.size() == 0) {
                pointsX.add(x);
                pointsY.add(y);
            }

            if (pointsX.size() > 0 && x - pointsX.get(pointsX.size() - 1) > .1f) {
                pointsX.add(x);
                pointsY.add(y);

                ellipse(x + WIDTH / 2, y + HEIGHT / 2, 3f, 3f);
                ellipse(x + WIDTH / 2, HEIGHT / 2 - y, 3f, 3f);
            }
        }

//        for (int i = 0; i < pointsX.size(); i++) {
//            float y = pointsY.get(i);
//            float x = pointsX.get(i);
//
//            float newY = y + cos(x / (float) Math.PI) * 2;
//            float newX = x + sin(x / (float) Math.PI) * 2;
//
//            ellipse(newX + WIDTH / 2, newY + HEIGHT / 2, 3f, 3f);
//            ellipse(newX + WIDTH / 2, HEIGHT / 2 - newY, 3f, 3f);
//        }
    }

    public void draw() {


    }
}
