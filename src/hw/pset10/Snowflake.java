package hw.pset10;

import processing.core.PApplet;

public class Snowflake {
    private float x, y, size, ySpeed, amps, alpha;
    private PApplet pApplet;

    public Snowflake(PApplet pApplet) {
        this.pApplet = pApplet;
        x = (float) Math.random() * P10E.WIDTH;
        y = (float) Math.random() * P10E.HEIGHT;
        size = (float) Math.random() * 6 + 5;
        ySpeed = (float) Math.pow(size / 12, 1.2f);
        amps = (float) (Math.random() * Math.PI);
        alpha = (size / 11) * 255;
    }

    public void move() {
        amps += .02;
        x += Math.sin(amps) / 3;
        y += ySpeed;

        if (y > P10E.HEIGHT) {
            x = (float) Math.random() * P10E.WIDTH;
            y = 0;
        }
    }

    public void draw() {
        pApplet.fill(255, 255, 255, alpha);
        pApplet.ellipse(x, y, size, size);
    }
}