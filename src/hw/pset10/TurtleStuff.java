package hw.pset10;

import TurtleGraphics.*;

public class TurtleStuff {
    public static void main(String[] args) {
        StandardPen p1 = new StandardPen();
        p1.move(50);
        p1.down();
        drawHexagon(p1);
        p1.up();

        RainbowPen p2 = new RainbowPen();
        p2.down();
        drawHexagon(p2);
        p2.up();

        WigglePen p3 = new WigglePen();
        p3.down();
        drawHexagon(p3);
        p3.up();

        WiggleRainbowPen p4 = new WiggleRainbowPen();
        p4.down();
        drawHexagon(p4);
        p4.up();
    }

    public static void drawHexagon(Pen pen) {
        for (int i = 0; i < 6; i++) {
            pen.turn(60);
            pen.move(60);
        }
    }
}
