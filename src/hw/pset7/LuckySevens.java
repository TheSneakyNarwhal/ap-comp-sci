package hw.pset7;

import java.util.Scanner;

public class LuckySevens {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Dice    die1   = new Dice();
        Dice    die2   = new Dice();
        int     dollars, count, maxDollars, countAtMax;
























        System.out.print("How many dollars do you have? ");
        dollars = reader.nextInt();

        maxDollars = dollars;
        countAtMax = 0;
        count = 0;

        while (dollars > 0) {
            count++;

            die1.roll();
            die2.roll();

            if (die1.getSide() + die2.getSide() == 7)
                dollars += 4;
            else
                dollars--;

            if (dollars > maxDollars) {
                maxDollars = dollars;
                countAtMax = count;
            }
        }

        System.out.println("You are broke after " + count + " rolls.\n" +
                "You should have quit after " + countAtMax +
                " rolls when you had $" + maxDollars + ".");
    }
}
