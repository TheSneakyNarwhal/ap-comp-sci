package hw.pset7;

import java.util.Random;

public class Dice {
    private Random random;
    private int    side;

    public Dice() {
        random = new Random();
        roll();
    }

    public void roll() {
        side = random.nextInt(6) + 1;
    }

    public int getSide() {
        return side;
    }
}
