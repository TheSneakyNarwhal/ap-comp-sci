package hw.pset7;

public class StudentTester {
    public static void main(String[] args) {

        Student student = new Student();
        student.setName("Jeff");
        student.setScore(1, 12);
        student.setScore(2, 50);
        student.setScore(3, 98);

        println(student.toString());

        student.setScore(3, 100);

        println(student.toString());

        println("");

        println("student score 1 == 12: " + (student.getScore(1) == 12));
        println("student score 2 == 50: " + (student.getScore(2) == 50));
        println("student score 3 == 100: " + (student.getScore(3) == 100));

        Student student2 = new Student("Jeff2", 35, 45, 75);
        println("student2 score 1 == 35: " + (student2.getScore(1) == 35));
        println("student2 score 2 == 45: " + (student2.getScore(2) == 45));
        println("student2 score 3 == 75: " + (student2.getScore(3) == 75));

        Student student3 = new Student(student2);
        println("student3 score 1 == 35: " + (student3.getScore(1) == 35));
        println("student3 score 2 == 45: " + (student3.getScore(2) == 45));
        println("student3 score 3 == 75: " + (student3.getScore(3) == 75));

        println("student valid: " + student.isValid());
        println("student2 valid: " + student2.isValid());
        println("student2 valid: " + student3.isValid());

        Student invalidStudent1 = new Student();
        println(invalidStudent1.toString());
        println("invalidStudent1 valid: " + invalidStudent1.isValid());

        Student invalidStudent2 = new Student("Jeff", -12, 5, 7);
        println(invalidStudent2.toString());
        println("invalidStudent2 valid: " + invalidStudent2.isValid());
    }

    public static void println(Object obj) {
        System.out.println(obj);
    }
}
