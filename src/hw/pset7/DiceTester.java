package hw.pset7;

public class DiceTester {
    public static void main(String[] args) {
        Dice dice = new Dice();

        for (int i = 0; i < 10; i++) {
            dice.roll();
            System.out.println("Roll: " + dice.getSide());
        }
    }
}
