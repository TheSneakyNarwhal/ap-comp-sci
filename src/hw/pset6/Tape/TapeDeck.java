package hw.pset6.Tape;
public class TapeDeck {
    private boolean canRecord;

    public TapeDeck() {
        canRecord = false;
    }

    public void enableRecording() {
        canRecord = true;
    }

    public void disableRecording() {
        canRecord = false;
    }

    public void playTape() {

    }
}
