package hw.pset6;

import java.util.Scanner;

public class P6B {
    public static void main(String[] args) {
        int num1 = promptForInt();
        int num2 = promptForInt();
        int lcm = 0;

        boolean done = false;

        while(!done) {
            lcm++;

            if(isMultiple(lcm,num1) && isMultiple(lcm,num2)) {
                done = true;
            }
        }
        System.out.println("The least common multiple is " + lcm);

        //the program will loop until the integer lcm is divisible by both num1 and num2
    }

    public static int promptForInt() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter an integer: ");
        return scanner.nextInt();
    }

    public static boolean isMultiple(int lcm, int num) {
        return lcm % num == 0;
    }
}
