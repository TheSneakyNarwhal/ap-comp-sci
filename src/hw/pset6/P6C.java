package hw.pset6;

import images.APImage;
import images.Pixel;

public class P6C {
    public static void main(String[] args) {
        APImage image = new APImage("smokey.jpg");

        int w = image.getImageWidth();
        int h = image.getImageHeight();
        int n = 4;

        // this makes the images checkered greyscale
        for (int i = 0; i < n; i += 2) {
            for (int j = 0; j < n; j += 2) {
                greyScale(image, i * w / n, j * h / n, (i + 1) * w / n, (j + 1) * h / n);
                greyScale(image, (i + 1) * w / n, (j + 1) * h / n, (i + 2) * w / n - 1, (j + 2) * h / n - 1);
            }
        }

        image.draw();
    }

    public static void greyScale(APImage image, int minX, int minY, int maxX, int maxY) {
        for (int x = minX; x < maxX; x++) {
            for (int y = minY; y < maxY; y++) {
                Pixel pixel = image.getPixel(x, y);

                int red = (int) Math.round(pixel.getRed() * 0.299);
                int green = (int) Math.round(pixel.getGreen() * 0.587);
                int blue = (int) Math.round(pixel.getBlue() * 0.114);
                int colorAverage = red + green + blue;

                pixel.setRed(colorAverage);
                pixel.setGreen(colorAverage);
                pixel.setBlue(colorAverage);
            }
        }

    }
}
