package hw.pset4;

import java.util.Scanner;

public class P4A {
    public static void main(String[] arg) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = scanner.nextInt();

        int count = 0;
        for (int i = 0; i <= number; i++) {
            count += i;
        }
        System.out.println(count);
    }
}
