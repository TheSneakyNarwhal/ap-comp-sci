package hw.pset4;

import java.util.Random;

public class P4C {
    public static void main(String[] args) {
        Random random = new Random();
        int count = 0;
        int iterations = 10000000;

        for (int i = 0; i < iterations; i++) {
            if (Math.pow(random.nextDouble(), 2) +  Math.pow(random.nextDouble(), 2) < 1) {
                count++;
            }
        }
        System.out.println("Count: " + count);
        System.out.println("PI estimate: " + (double) count / iterations * 4);
    }
}
