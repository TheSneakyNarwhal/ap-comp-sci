package hw.pset4;

import java.util.Random;

public class P4D {
    public static void main(String[] args) {
        Random random = new Random();
        for (int i = 1; i <= 10; i++) {
            int square = (int) Math.pow(i, 2);
            int randNum = random.nextInt(square - i + 1) + i;
            System.out.println(i + "\t" + square + "\t" + randNum);
        }
    }
}
