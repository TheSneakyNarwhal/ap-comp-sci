package hw.pset4;

import java.util.Scanner;

public class P4B {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number between 1 and 10: ");
        int input = scanner.nextInt();
        while (input <= 1 || input >= 10) {
            System.out.println("That is not a valid number, try again.");
            System.out.print("Enter a number between 1 and 10: ");
            input = scanner.nextInt();
        }
        System.out.println("Your number is: " + input);
    }
}
