package hw.pset8;

public class BoardTester {
    public static void main(String[] args) {
        Board board = new Board();

        board.markSquare(2, "O");

        System.out.println(board.getMark(2));

        board.markSquare(1, "X");
        board.markSquare(5, "X");
        board.markSquare(9, "X");

        System.out.println("winner: " + board.checkForWin());
    }
}
