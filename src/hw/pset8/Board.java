package hw.pset8;

public class Board {
    private String[][] board;

    public Board() {
        board = new String[3][3];
        clearBoard();
    }

    public void clearBoard() {
        int count = 0;
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                count++;
                board[y][x] = String.valueOf(count);
            }
        }
    }

    public int getNumFullSquares() {
        int full = 0;
        for (int i = 1; i <= 9; i++) {
            if (getMark(i).equals("X") || getMark(i).equals("O"))
                full++;
        }
        return full;
    }

    public boolean markSquare(int position, String mark) {
        mark = mark.toUpperCase();

        if (getMark(position).equals("X") ||
                getMark(position).equals("O") ||
                position < 1 ||
                position > 9) {
            return false;
        }

        position--;
        board[position / 3][position % 3] = mark;
        return true;
    }

    public String checkForWin() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0].equals(board[i][1]) && board[i][1].equals(board[i][2]))
                return board[i][0];
            else if (board[0][i].equals(board[1][i]) && board[1][i].equals(board[2][i]))
                return board[0][i];
        }

        if (board[0][0].equals(board[1][1]) && board[1][1].equals(board[2][2]))
            return board[0][0];
        else if (board[0][2].equals(board[1][1]) && board[1][1].equals(board[2][0]))
            return board[0][2];

        return null;
    }

    public String getMark(int position) {
        position--;
        return board[position / 3][position % 3];
    }
}
