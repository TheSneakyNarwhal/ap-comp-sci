package hw.ari;

import java.util.Scanner;
        import java.io.*;

public class P9E
{
    public static void main(String[] args)throws IOException
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a word: ");
        String word = scanner.nextLine();
        System.out.println("Enter a filename: ");
        String fileName = scanner.nextLine();
        System.out.println(containsCheck(fileName, word));

    }

    public static boolean containsCheck(String fileName, String word) throws IOException
    {
        Scanner reader = new Scanner(new File(fileName));
        while(reader.hasNext())
        {
            String line = reader.nextLine();
            if(line.contains(word))
            {
                return true;
            }
        }
        return false;
    }
}

