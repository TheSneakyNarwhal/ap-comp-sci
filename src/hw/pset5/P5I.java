package hw.pset5;

import images.APImage;
import images.Pixel;

import java.util.Scanner;

public class P5I {
    public static void main(String[] args) {
        APImage image = new APImage("smokey.jpg");

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter resize factor: ");
        int     resizeFactor = scanner.nextInt();

        int width        = image.getImageWidth();
        int height       = image.getImageHeight();

        APImage resizedImage = new APImage(width * resizeFactor, height * resizeFactor);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int newX = x * resizeFactor;
                int newY = y * resizeFactor;

                Pixel pixel = image.getPixel(x, y);
                for (int fillX = 0; fillX < resizeFactor; fillX++) {
                    for (int fillY = 0; fillY < resizeFactor; fillY++) {
                        resizedImage.setPixel(newX + fillX, newY + fillY, pixel);
                    }
                }
            }
        }
        resizedImage.draw();
    }
}
