package hw.pset5;

import images.APImage;
import images.Pixel;

import java.util.Scanner;

public class P5H {
    public static void main(String[] args) {
        APImage image        = new APImage("smokey.jpg");

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter resize factor: ");
        int     resizeFactor = scanner.nextInt();

        int width  = image.getImageWidth();
        int height = image.getImageHeight();

        APImage resizedImage = new APImage(width / resizeFactor, height / resizeFactor);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int newX = x / resizeFactor;
                int newY = y / resizeFactor;

                Pixel pixel = image.getPixel(x, y);
                resizedImage.setPixel(newX, newY, pixel);
            }
        }

        resizedImage.draw();
    }
}