package hw.pset5;

import images.APImage;
import images.Pixel;

public class P5A {
    public static void main(String[] args) {
        APImage image = new APImage(500, 300);
        for (Pixel pixel : image) {
            pixel.setRed(0);
            pixel.setGreen(255);
            pixel.setBlue(0);
        }
        image.draw();
    }
}