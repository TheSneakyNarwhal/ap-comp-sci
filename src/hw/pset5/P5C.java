package hw.pset5;

import images.APImage;
import images.Pixel;

public class P5C {
    public static void main(String[] args) {
        APImage image = new APImage(500, 300);

        for (int x = 0, width = image.getImageWidth(); x < width; x++) {
            for (int y = 0, height = image.getImageHeight(); y < height; y++) {
                if (x == 0 || y == 0 || x == width - 1 || y == height - 1) {
                    Pixel pixel = image.getPixel(x, y);
                    pixel.setBlue(255);
                }
            }
        }
        image.draw();
    }
}
