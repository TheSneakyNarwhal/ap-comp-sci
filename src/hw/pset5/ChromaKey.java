package hw.pset5;

import images.APImage;
import images.Pixel;

public class ChromaKey {
    public static void main(String[] args) {
        APImage image  = new APImage("cat.jpg");
        int     width  = image.getImageWidth();
        int     height = image.getImageHeight();

        APImage bgImage = new APImage("candyland.jpg");

        int lowerLimit = 90;
        int upperLimit = 125;

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Pixel pixel = image.getPixel(x, y);

                int red = pixel.getRed();
                int green = pixel.getGreen();
                int blue = pixel.getBlue();

                // works, but a lot of artifacts
                /*
                if (red > 10 && blue > 10) {
                    bgImage.setPixel(x, y, pixel);
                }
                */

                // a more accurate way
                double hue = rgbToHue(red, green, blue);
                if (hue < lowerLimit || hue > upperLimit) {
                    double alpha = 1;
                    Pixel bgPixel = bgImage.getPixel(x, y);
                    red = applyAlphaToColorValue(red, bgPixel.getRed(), alpha);
                    green = applyAlphaToColorValue(green, bgPixel.getGreen(), alpha);
                    blue = applyAlphaToColorValue(blue, bgPixel.getBlue(), alpha);

                    pixel.setRed(red);
                    pixel.setGreen(green);
                    pixel.setBlue(blue);
                    bgImage.setPixel(x, y, pixel);
                }

            }
        }

        bgImage.draw();
    }

    // Formula from
    // http://www.rapidtables.com/convert/color/rgb-to-hsl.htm
    public static double rgbToHue(int red, int green, int blue) {
        // change range of color values to [0, 1]
        double r = (double) red / 255;
        double g = (double) green / 255;
        double b = (double) blue / 255;

        //find the min and max values colors
        double maxColor = Math.max(r, Math.max(g, b));
        double minColor = Math.min(r, Math.min(g, b));

        double delta = maxColor - minColor;

        double hue;
        // There are different formulas for calculating hue for each color

        if (maxColor == r) {
            hue = (g - b) / delta % 6;
        } else if (maxColor == g) {
            hue = 2 + (b - r) / delta;
        } else {
            hue = 4 + (r - g) / delta;
        }
        // convert to degrees
        hue *= 60;

        // fix negative values
        if (hue < 0) {
            hue += 360;
        }

        return hue;
    }

    public static int applyAlphaToColorValue(int value, int bgValue, double alpha) {
        return (int)((value * alpha) + (bgValue * (1.0 - alpha)));
    }
}
