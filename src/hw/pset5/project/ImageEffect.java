package hw.pset5.project;

import images.APImage;
import images.Pixel;

public class ImageEffect {
    public static APImage blackAndWhite(APImage image) {

        for (Pixel pixel : image) {
            int red = (int) Math.round(pixel.getRed() * 0.299);
            int green = (int) Math.round(pixel.getGreen() * 0.587);
            int blue = (int) Math.round(pixel.getBlue() * 0.114);
            int colorAverage = red + green + blue;

            pixel.setRed(colorAverage);
            pixel.setGreen(colorAverage);
            pixel.setBlue(colorAverage);
        }

        return image;
    }

    public static APImage invert(APImage image) {

        for (Pixel pixel : image) {
            int red = Math.abs(pixel.getRed() - 255);
            int green = Math.abs(pixel.getGreen() - 255);
            int blue = Math.abs(pixel.getBlue() - 255);

            pixel.setRed(red);
            pixel.setGreen(green);
            pixel.setBlue(blue);
        }

        return image;
    }

    public static APImage sepia(APImage image) {
        image = blackAndWhite(image);

        for (Pixel pixel : image) {
            int red = pixel.getRed() + 30 * 2;
            int green = pixel.getGreen() + 20;

            if (red > 255) {
                red = 255;
            }
            if (green > 255) {
                green = 255;
            }

            pixel.setRed(red);
            pixel.setGreen(green);
        }

        return image;
    }

    public static APImage mirrorTopBot(APImage image) {

        int height = image.getImageHeight();
        for (int x = 0, width = image.getImageWidth(); x < width; x++) {
            for (int y = 0, halfHeight = height / 2; y < halfHeight; y++) {
                Pixel pixel = image.getPixel(x, y);

                image.setPixel(x, height - y - 1, pixel);
            }
        }

        return image;
    }

    public static APImage mirrorLeftRight(APImage image) {

        int width = image.getImageWidth();
        for (int y = 0, height = image.getImageHeight(); y < height; y++) {
            for (int x = 0, halfWidth = width / 2; x < halfWidth; x++) {
                Pixel pixel = image.getPixel(x, y);

                image.setPixel(width - x - 1, y, pixel);
            }
        }

        return image;
    }

    public static APImage multiply(APImage image, double factor) {

        for (Pixel pixel : image) {
            int red = checkColorValue((int) (pixel.getRed() * factor));
            int green = checkColorValue((int) (pixel.getGreen() * factor));
            int blue = checkColorValue((int) (pixel.getBlue() * factor));

            pixel.setRed(red);
            pixel.setGreen(green);
            pixel.setBlue(blue);
        }

        return image;
    }

    private static int checkColorValue(int color) {
        if (color > 255) {
            return 255;
        } else if (color < 0) {
            return 0;
        } else {
            return color;
        }
    }
}
