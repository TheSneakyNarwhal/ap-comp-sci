package hw.pset5.project;

import images.APImage;
import images.Pixel;

public class Collage extends APImage {
    APImage originalImage;

    public Collage(APImage originalImage, int rows, int columns) {
        super(originalImage.getImageWidth() * columns, originalImage.getImageHeight() * rows);

        this.originalImage = originalImage;
    }

    public void applyFilters() {
        drawImageAt(originalImage, 0, 0);

        int width = originalImage.getImageWidth();
        int height = originalImage.getImageHeight();

        APImage bAndG = ImageEffect.blackAndWhite(cloneImage(originalImage));
        drawImageAt(bAndG, 0, height);

        APImage inverted = ImageEffect.invert(cloneImage(originalImage));
        drawImageAt(inverted, 0, height * 2);

        APImage sepia = ImageEffect.sepia(cloneImage(originalImage));
        drawImageAt(sepia, width, 0);

        APImage mirrorHor = ImageEffect.mirrorTopBot(cloneImage(originalImage));
        APImage multiply = ImageEffect.multiply(mirrorHor, 1.25);
        drawImageAt(multiply, width, height);

        APImage mirrorVer = ImageEffect.mirrorLeftRight(cloneImage(originalImage));
        drawImageAt(mirrorVer, width, height * 2);
    }

    public static void main(String[] args) {
        APImage originalImage = new APImage("niccage.jpg");

        Collage collage = new Collage(originalImage, 3, 2);
        collage.applyFilters();
        collage.draw();
    }

    private void drawImageAt(APImage image, int anchorX, int anchorY) {
        for (int x = 0, width = image.getImageWidth(); x < width; x++) {
            for (int y = 0, height = image.getImageHeight(); y < height; y++) {
                setPixel(anchorX + x, anchorY + y, image.getPixel(x, y));
            }
        }
    }

    private static APImage cloneImage(APImage image) {
        int width = image.getImageWidth();
        int height = image.getImageHeight();

        APImage cloneImage = new APImage(width, height);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Pixel pixel = image.getPixel(x, y).clone();
                cloneImage.setPixel(x, y, pixel);
            }
        }

        return cloneImage;
    }

}
