package hw.pset5;

import images.APImage;
import images.Pixel;

public class P5F {
    public static void main(String[] args) {
        APImage image = new APImage("west-puzzle.png");

        for (Pixel pixel : image) {
            int blue = pixel.getBlue();
            if (blue < 16) {
                blue *= 16;
            } else {
                blue = 0;
            }

            pixel.setRed(blue);
            pixel.setBlue(0);
            pixel.setGreen(0);
        }
        image.draw(); // golden gate bridge
    }
}
