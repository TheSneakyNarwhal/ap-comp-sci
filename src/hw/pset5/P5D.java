package hw.pset5;

import images.APImage;
import images.Pixel;

public class P5D {
    public static void main(String[] args) {
        APImage image = new APImage("iron-puzzle.png");

        for (Pixel pixel : image) {
            int red = pixel.getRed() * 10;

            pixel.setRed(red);
            pixel.setGreen(0);
            pixel.setBlue(0);
        }
        image.draw(); //Eiffel tower
    }
}
