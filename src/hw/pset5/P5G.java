package hw.pset5;

import images.APImage;
import images.Pixel;

import java.util.Scanner;

public class P5G {
    public static void main(String[] args) {
        APImage image = new APImage("smokey.jpg");

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter filter values separated by spaces: ");

        int redFilter = scanner.nextInt();
        int greenFilter = scanner.nextInt();
        int blueFilter = scanner.nextInt();

        for (Pixel pixel : image) {
            int red = applyFilter(pixel.getRed(), redFilter);
            int green = applyFilter(pixel.getGreen(), greenFilter);
            int blue = applyFilter(pixel.getBlue(), blueFilter);

            pixel.setRed(red);
            pixel.setGreen(green);
            pixel.setBlue(blue);
        }
        image.draw();
    }

    public static int applyFilter(int val, int filter) {
        int sum = val + filter;

        if (sum > 255) {
            return 255;
        } else if (sum < 0) {
            return 0;
        }
        return sum;
    }
}