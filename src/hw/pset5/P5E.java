package hw.pset5;

import images.APImage;
import images.Pixel;

public class P5E {
    public static void main(String[] args) {
        APImage image = new APImage("copper-puzzle.png");

        for (Pixel pixel : image) {
            int green = pixel.getGreen() * 20;
            int blue = pixel.getBlue() * 20;

            pixel.setRed(0);
            pixel.setGreen(green);
            pixel.setBlue(blue);
        }
        image.draw(); // statue of liberty
    }
}
