package hw.pset5;

import images.APImage;
import images.Pixel;

public class P5B {
    public static void main(String[] args) {
        APImage image = new APImage("smokey.jpg");

        for (Pixel pixel : image) {
            int red = (int) Math.round(pixel.getRed() * 0.299);
            int green = (int) Math.round(pixel.getGreen() * 0.587);
            int blue = (int) Math.round(pixel.getBlue() * 0.114);
            int colorAverage = red + green + blue;

            pixel.setRed(colorAverage);
            pixel.setGreen(colorAverage);
            pixel.setBlue(colorAverage);
        }

        image.draw();
    }
}
