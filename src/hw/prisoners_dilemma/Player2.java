package hw.prisoners_dilemma;

public class Player2 implements PDPlayer {
    public String moves = "";

    public String chooseCorD(String opponentsLastMove) {
        moves += opponentsLastMove;

        int movesLen = moves.length();
        for (int i = movesLen; i > 0; i--) {
            int shift = movesLen - i + 1;
            String sub = moves.substring(0, shift);
            String patt = moves.substring(shift, movesLen);

            int index = sub.indexOf(patt);
            if (index != -1) {
                index += movesLen - shift;
                return moves.substring(index, index + 1);
            }
        }

        return "d";
    }

    public String getAuthor() {
        return "Derrick Liu";
    }

    public String toString() {
        return "Finds patterns";
    }
}
