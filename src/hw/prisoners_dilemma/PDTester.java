package hw.prisoners_dilemma;

public class PDTester {
    static int p1Score = 0;
    static int p2Score = 0;

    public static void main(String[] args) {
        Player2 p1 = new Player2();
        Player2 p2 = new Player2();

        String p1Choice = p1.chooseCorD("");
        String p2Choice = p2.chooseCorD("");

        for (int i = 0; i < 200; i++) {
            System.out.println("=======================================");
            System.out.println("p1 moves: " + p1.moves);
            System.out.println("p2 moves: " + p2.moves);
            System.out.println("\tp1 choice: " + p1Choice + ", p2 choice: " + p2Choice);
            if (p1Choice.equals("c") && p2Choice.equals("c")) {
                p1Score++;
                p2Score++;
            } else if (p1Choice.equals("d") && p2Choice.equals("c")) {
                p1Score += 2;
                p2Score--;
            } else if (p2Choice.equals("d") && p1Choice.equals("c")) {
                p2Score += 2;
                p1Score--;
            }
            System.out.println("\tp1: " + p1Score + ", p2: " + p2Score);
            System.out.println("=======================================");
            String lastP1Choice = p1Choice;
            p1Choice = p1.chooseCorD(p2Choice);
            p2Choice = p2.chooseCorD(lastP1Choice);
        }
    }
}
