package hw.prisoners_dilemma;

public class Prisoner implements PDPlayer {
    public String chooseCorD(String opponentsLastMove) {
        return "d";
    }

    public String getAuthor() {
        return "Derrick Liu";
    }

    public String toString() {
        return "Always defect";
    }
}
