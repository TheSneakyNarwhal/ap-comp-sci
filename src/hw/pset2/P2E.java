package hw.pset2;

import java.util.Scanner;

public class P2E {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print("Enter the first time: ");
        final int firstTime = scanner.nextInt();
        print("Enter the second time: ");
        final int secondTime = scanner.nextInt();

        int difference = secondTime - firstTime;
        int minutes1   = 60 - (firstTime % 100);
        int minutes2   = secondTime % 100;
        if (difference < 0) {
            difference += 2400;
        }

        println(difference / 100 + " hours and " + (minutes1 + minutes2) + " minutes");
    }

    public static void print(Object object) {
        System.out.print(object);
    }

    public static void println(Object object) {
        System.out.println(object);
    }
}
