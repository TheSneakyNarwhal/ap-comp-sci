package hw.pset2;

import java.util.Scanner;

public class P2C {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print("Please enter your name: ");
        final String name = scanner.nextLine();

        println("Greeting and salutations, " + name + "!");
    }

    public static void print(Object object) {
        System.out.print(object);
    }

    public static void println(Object object) {
        System.out.println(object);
    }
}
