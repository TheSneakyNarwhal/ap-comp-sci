package hw.pset2;

import java.util.Scanner;

public class P2A {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print("Type the first integer: ");
        final int firstInteger = scanner.nextInt();
        print("Type the second integer: ");
        final int secondInteger = scanner.nextInt();

        println("Sum: " + firstInteger + secondInteger);
        println("Product: " + firstInteger * secondInteger);
        println("Difference: " + (firstInteger - secondInteger));
        println("Quotient: " + ((double) firstInteger / secondInteger));
        println("Average: " + ((double) firstInteger + secondInteger) / 2);
    }

    public static void print(Object object) {
        System.out.print(object);
    }

    public static void println(Object object) {
        System.out.println(object);
    }

}
