package hw.pset2;

import java.util.Scanner;

public class P2B {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print("Type two numbers you would like to divide, separated by a space: ");
        final int firstInteger  = scanner.nextInt();
        final int secondInteger = scanner.nextInt();

        double quotient = (double) firstInteger / secondInteger;

        print(firstInteger + " divided by " + secondInteger + " is " + quotient);
        print(" OR ");
        println((int) quotient + " Remainder " + firstInteger % secondInteger);
    }

    public static void println(Object object) {
        System.out.println(object);
    }

    public static void print(Object object) {
        System.out.print(object);
    }

}
