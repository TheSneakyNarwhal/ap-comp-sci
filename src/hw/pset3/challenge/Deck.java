package hw.pset3.challenge;

import java.util.ArrayList;
import java.util.Random;

public class Deck extends ArrayList<Card> {
    public Deck() {
        for (int suit = 0; suit < 4; suit++) {
            for (int rank = 0; rank < 13; rank++) {
                Card card = new Card(suit, rank);
                add(card);
            }
        }
    }

    public void shuffle() {
        Random random = new Random();

        Card temp;
        for (int i = 0, length = size(); i < length; i++) {
            int randomNum = random.nextInt(length);
            temp = get(i);
            set(i, get(randomNum));
            set(randomNum, temp);
        }
    }
}