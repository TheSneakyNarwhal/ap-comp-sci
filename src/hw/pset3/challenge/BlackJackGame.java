package hw.pset3.challenge;

import java.util.ArrayList;
import java.util.Scanner;

public class BlackJackGame {
    ArrayList<Player> players = new ArrayList<>();
    Deck              deck    = new Deck();
    final Scanner scanner = new Scanner(System.in);
    int playersStanding = 0;

    public BlackJackGame(String[] playerNames) {
        for (String playerName : playerNames) {
            Player player = new Player(playerName);
            players.add(player);
        }
        deck.shuffle();

        players.forEach(this::dealToPlayer);
        players.forEach(this::dealToPlayer);

        gameLogic();
    }

    private void gameLogic() {
        while (playersStanding < players.size()) {
            players.stream().filter(player -> !player.stand).forEach(player -> {
                int handTotal = player.getHandTotal();
                if (handTotal < 22) {
                    System.out.println("====================================================");
                    System.out.println("Player " + player.id + ", what would you like to do?");
                    player.printHandAndTotal();
                    System.out.print("Hit or Stand?: ");
                    switch (scanner.nextLine().toLowerCase()) {
                        case "hit":
                            dealToPlayer(player);
                            break;
                        case "stand":
                            playersStanding++;
                            player.stand = true;
                            break;
                        default:
                            System.out.println("Error, unrecognized decision");
                            break;
                    }
                } else {
                    System.out.println("Player " + player.id + " busted!");
                    player.printHandAndTotal();

                    playersStanding++;
                    player.stand = true;
                    player.busted = true;
                }

            });
        }
        Player[] winner = new Player[2];
        players.stream().filter(player -> !player.busted).forEach(player -> {
            if (winner[0] == null) {
                winner[0] = player;
            } else {
                int total1 = player.getHandTotal();
                int total2 = winner[0].getHandTotal();
                if (total1 > total2) {
                    winner[0] = player;
                } else if (total1 == total2 && player.hand.size() == winner[0].hand.size()) {
                    winner[1] = winner[0];
                }
            }
        });

        if (winner[1] != null) {
            System.out.println("Its a tie between player: " + winner[0].id + " and " + winner[1].id);
        } else if (winner[0] != null) {
            System.out.println("The winner is player: " + winner[0].id);
            winner[0].printHandAndTotal();
        } else {
            System.out.println("Error in calculating winner!");
        }

    }

    public void dealToPlayer(Player player) {
        if (!deck.isEmpty()) {
            Card card = deck.remove(deck.size() - 1);
            player.hand.add(card);
            System.out.println("Player " + player.id + " is dealt a " + card.toString());
        } else {
            System.out.println("Error, deck is empty!");
            System.exit(-1);
        }
    }
}
