package hw.pset3.challenge;

public class Card {
    final int _suit;
    final int _rank;

    static final String[] SUITS      = {"Spades", "Hearts", "Diamonds", "Clubs"};
    //static final String[] SUIT_ICONS = {"?", "?", "?", "?"};
    static final String[] RANKS      = {"Ace", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};

    public Card(int suit, int rank) {
        this._suit = suit;
        this._rank = rank;
    }

    public String getSuit() {
        return SUITS[_suit];
    }

    //public String getSuitIcon() {
    //    return SUIT_ICONS[_suit];
    //}

    public String getRank() {
        return RANKS[_rank];
    }

    public int getNumber() {
        return _rank;
    }

    @Override
    public String toString() {
        return getRank() + " of " + getSuit();
    }
}
