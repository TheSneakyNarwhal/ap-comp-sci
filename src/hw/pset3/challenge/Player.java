package hw.pset3.challenge;

import java.util.ArrayList;

public class Player {
    public final String id;
    public boolean         stand  = false;
    public boolean         busted = false;
    public ArrayList<Card> hand   = new ArrayList<>();

    public Player(String id) {
        this.id = id;
    }

    public void printHand() {
        for (int i = 0, length = hand.size(); i < length; i++) {
            Card card = hand.get(i);
            if (i == 0) {
                System.out.print("[ " + card.toString() + ", ");
            } else if (i == length - 1) {
                System.out.println(card.toString() + "]");
            } else {
                System.out.print(card.toString() + ", ");
            }
        }
    }

    public int getHandTotal() {
        int aces  = 0;
        int total = 0;
        for (Card card : hand) {
            switch (card.getRank()) {
                case "Ace":
                    aces++;
                    total += 11;
                    break;
                case "Jack":
                case "Queen":
                case "King":
                    total += 10;
                    break;
                default:
                    total += card.getNumber();
                    break;
            }
        }

        while (total > 21 && aces > 0) {
            total -= 10;
            aces--;
        }
        return total;
    }

    public void printHandAndTotal() {
        System.out.print("\tHand: ");
        printHand();
        System.out.println("\tTotal: " + getHandTotal());
    }
}