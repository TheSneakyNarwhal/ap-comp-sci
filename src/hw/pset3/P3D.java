package hw.pset3;

import java.util.Scanner;

public class P3D {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a fraction: ");
        double fraction = scanner.nextDouble();
        System.out.println("Fractional part: " + fraction % 1);
    }
}
