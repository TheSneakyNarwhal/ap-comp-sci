package hw.pset3;

import java.util.Random;
import java.util.Scanner;

public class P3F {

    public static void main(String[] args) {
        Scanner scanner      = new Scanner(System.in);
        Random  random       = new Random();
        int     randomNumber = random.nextInt(10) + 1;
        System.out.print("Try to guess the secret number between 1 and 10: ");
        while (scanner.nextInt() != randomNumber) {
            System.out.println("That's not it!");
            System.out.print("Try to guess the secret number between 1 and 10: ");
        }
        System.out.println("You got it!");
    }

}
