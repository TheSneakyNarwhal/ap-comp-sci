package hw.pset3;

import java.util.Scanner;

public class P3E {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Choose a program to run (P3B, P3C, P3D): ");
        switch (scanner.nextLine().toLowerCase()) {
            case "p3b":
                p2B();
                break;
            case "p3c":
                p2C();
                break;
            case "p3d":
                p2D();
                break;
            default:
                System.out.println("Error: program not found!");
        }
    }

    public static void p2B() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = scanner.nextInt();

        if (number % 2 == 0) {
            System.out.println("This is an even number");
        } else {
            System.out.print("This is an odd number");
        }
    }

    public static void p2C() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 3 numbers separated by spaces: ");
        int first = scanner.nextInt();
        int second = scanner.nextInt();
        int third = scanner.nextInt();

        if ((first < second && second < third) || (first > second && second > third)) {
            System.out.println("in order");
        } else {
            System.out.println("not in order");
        }
    }

    public static void p2D() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a fraction: ");
        double fraction = scanner.nextDouble();
        System.out.println("Fractional part: " + fraction % 1);
    }

}
