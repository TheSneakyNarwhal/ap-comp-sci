package hw.pset3;

import java.util.Scanner;

public class P3C {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 3 numbers separated by spaces: ");
        int first = scanner.nextInt();
        int second = scanner.nextInt();
        int third = scanner.nextInt();

        if ((first <= second && second <= third) || (first >= second && second >= third)) {
            System.out.println("in order");
        } else {
            System.out.println("not in order");
        }
    }

}
