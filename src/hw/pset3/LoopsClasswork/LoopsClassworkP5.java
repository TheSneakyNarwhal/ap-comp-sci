package hw.pset3.LoopsClasswork;

import java.util.Scanner;

public class LoopsClassworkP5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of iterations: ");
        int iterations = scanner.nextInt();
        double pi = 0;

        for (int i = 1; i <= iterations; i++) {
            double delta = (double) 4/ (2 * i - 1);

            if (i % 2 == 0) {
                pi -= delta;
            } else {
                pi += delta;
            }
        }

        System.out.println("PI: " + pi);
    }
}
