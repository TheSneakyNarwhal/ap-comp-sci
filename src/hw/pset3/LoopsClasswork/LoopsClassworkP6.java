package hw.pset3.LoopsClasswork;

import java.util.Random;
import java.util.Scanner;

public class LoopsClassworkP6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number between 0 and 1: ");

        double input = scanner.nextDouble();

        Random random = new Random();
        int count = 0;

        double guess = -1.0;
        while(guess <= input) {
            guess = random.nextDouble();
            count++;
        }
        System.out.println("Took " + count + " tries to generate a number bigger than " + input);
    }
}
