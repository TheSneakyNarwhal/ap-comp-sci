package hw.pset3.LoopsClasswork;

public class LoopsClassworkP1 {
    public static void main(String[] args) {
        for (int i = 0; i <= 10; i++) {
            System.out.println("Number: " + i + ", Root: " + Math.sqrt(i));
        }
    }
}
