package hw.pset3.LoopsClasswork;

public class LoopsClassworkP2 {
    public static void main(String[] args) {
        int multiplesOfThree = 0;
        for (int i = 0; i < 1000; i++) {
            if (i % 3 == 0) {
                multiplesOfThree++;
            }
        }
        System.out.println("Multiples of three in 1000: " + multiplesOfThree);
    }
}
