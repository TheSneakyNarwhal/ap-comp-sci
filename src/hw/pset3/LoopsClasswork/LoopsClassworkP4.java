package hw.pset3.LoopsClasswork;

public class LoopsClassworkP4 {
    public static void main(String[] args) {
        int count = 0;
        while (true) {
            count += 5000;
            System.out.println(count);
            if (count < 0) {
                break;
            }
        }
    }
}
