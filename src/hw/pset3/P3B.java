package hw.pset3;

import java.util.Scanner;

public class P3B {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = scanner.nextInt();

        if (number % 2 == 0) {
            System.out.println("This is an even number");
        } else {
            System.out.print("This is an odd number");
        }
    }

}
