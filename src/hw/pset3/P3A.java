package hw.pset3;

import java.util.Scanner;

public class P3A {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the first number: ");
        int firstNumber = scanner.nextInt();
        System.out.print("Enter second number: ");
        int secondNumber = scanner.nextInt();

        if (secondNumber > firstNumber) {
            int temp = firstNumber;
            firstNumber = secondNumber;
            secondNumber = temp;
        }
        System.out.println("Quotient: " + firstNumber/secondNumber + " Remainder: " + firstNumber % secondNumber);
    }
}