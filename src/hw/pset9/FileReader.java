package hw.pset9;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileReader {
    public static void main(String[] args) throws IOException {
        File file = new File("output.txt");
        Scanner scanner = new Scanner(file);

        while(scanner.hasNext()) {
            System.out.println(scanner.nextLine());
        }
    }
}
