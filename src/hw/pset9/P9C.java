package hw.pset9;

public class P9C {

    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i + " factorial: " + factorial(i));
        }

        for (int i = 1; i <= 10; i++) {
            int factorial = 1;
            for (int j = 1; j <= i; j++) {
                factorial *= j;
            }
            System.out.println(factorial);
        }
    }

    public static int factorial(int x) {
        if (x == 0) return 1;
        if (x < 0) return -1;

        for (int i = x - 1; i > 0; i--) {
            x *= i;
        }
        return x;
    }
}
