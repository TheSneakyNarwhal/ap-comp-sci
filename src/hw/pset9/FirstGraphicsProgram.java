package hw.pset9;

import processing.core.PApplet;

public class FirstGraphicsProgram extends PApplet {

    int x;

    public static void main(String[] args) {
        PApplet.main(new String[]{"hw.pset9.FirstGraphicsProgram"});
    }

    public void setup() {
        size(400, 400);
        ellipseMode(CORNER);
    }

    public void draw() {
        stroke(255, 0, 0);

        if (mousePressed)
            line(pmouseX, pmouseY, mouseX, mouseY);
    }

}
