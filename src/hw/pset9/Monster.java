package hw.pset9;

import processing.core.PApplet;

public class Monster extends PApplet {

    public static void main(String[] args) {
        PApplet.main(new String[]{"hw.pset9.Monster"});
    }

    public void setup() {
        size(400, 400);
        ellipseMode(CORNER);
        frameRate(60);
    }

    public void draw() {
        background(0);
        int offset = 170;
        drawOcto(mouseX, mouseY);
        drawOcto(mouseX + offset, mouseY);
        drawOcto(mouseX - offset, mouseY);
        drawOcto(mouseX + offset, mouseY + offset);
        drawOcto(mouseX - offset, mouseY + offset);
        drawOcto(mouseX + offset, mouseY - offset);
        drawOcto(mouseX - offset, mouseY - offset);
        drawOcto(mouseX, mouseY - offset);
        drawOcto(mouseX, mouseY + offset);
    }

    public void drawOcto(int x, int y) {


        stroke(231, 76, 60);
        strokeWeight(25);

        int tentMoveOffset = (int)(Math.sin(frameCount / 4) * 4);
        line(x, y, x - 45 - tentMoveOffset * 2, y + 60 + tentMoveOffset / 2);
        line(x, y, x - 17 - tentMoveOffset, y + 75 + tentMoveOffset / 2);
        line(x, y, x + 45 + tentMoveOffset * 2, y + 60 + tentMoveOffset / 2);
        line(x, y, x + 17 + tentMoveOffset, y + 75 + tentMoveOffset / 2);

        int eyeSize = 70;
        int eyeDist = 8;
        int eyeYOffset = 10;

        int bodySize = 130;

        //body
        noStroke();
        fill(231, 76, 60);
        ellipse(x - bodySize / 2, y - bodySize / 2, bodySize, bodySize);

        //eye balls
        fill(255);
        ellipse(x - eyeSize - eyeDist, y - eyeSize + eyeYOffset, eyeSize, eyeSize);
        ellipse(x + eyeDist, y - eyeSize + eyeYOffset, eyeSize, eyeSize);

        //eyes
        fill(0);
        int blackEyeSize = 30;
        eyeYOffset = -10;
        int eyeMoveOffset = (int)(Math.sin(frameCount) * 3);
        ellipse(x - blackEyeSize - eyeDist + eyeMoveOffset, y - blackEyeSize + eyeYOffset, blackEyeSize, blackEyeSize);
        ellipse(x + eyeDist + eyeMoveOffset, y - blackEyeSize + eyeYOffset, blackEyeSize, blackEyeSize);

        //mouth
        int mouthSize = 20;
        ellipse(x - mouthSize / 2, y, mouthSize, mouthSize);
    }

}
