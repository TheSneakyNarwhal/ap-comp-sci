import java.util.Scanner;
public class Ari
{
    public static void main(String[] args)
    {
//        Scanner reader = new Scanner(System.in);
//        System.out.println("Enter a number: ");
//        int x = reader.nextInt();
//        System.out.println(factorial(x));

        for (int i = 1; i <= 10; i++) {
            System.out.println(factorial(i));
        }
    }

    public static int factorial(int x)
    {
        if(x == 0)
        {
            return 1;
        }
        if(x < 0)
        {
            return -1;
        }
        for(int i = x - 1; i > 0; i--)
        {
            x *= i;
        }
        return x;
    }
}
        