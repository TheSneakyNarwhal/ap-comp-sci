package hw.pset9;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FileWriter {
    public static void main(String[] args) throws IOException {
        File f = new File("output.txt");
        PrintWriter writer = new PrintWriter(f);
        writer.println("This should be writing to my output file.");
        writer.println("Here's another line");
        writer.close();
    }
}
