package hw.pset9.pong;

import processing.core.PApplet;

public class Paddle {
    public static int HEIGHT = 100;
    public static int WIDTH = 15;

    static int MOVE_SPEED = 10;

    private PApplet pApplet;

    private int x;
    private int y;

    private boolean upPressed = false;
    private boolean downPressed = false;

    public Paddle(PApplet pApplet, int x) {
        this.pApplet = pApplet;
        this.x = x;
        this.y = pApplet.height / 2 - HEIGHT / 2;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void move() {
        int moveDir = 0;

        if (upPressed) moveDir--;
        if (downPressed) moveDir++;

        int newY = y + MOVE_SPEED * moveDir;
        if (newY >= 0 && newY <= pApplet.height - HEIGHT) {
            y = newY;
        }
    }

    public void display() {
        pApplet.fill(255);
        pApplet.rect(x, y, WIDTH, HEIGHT);
    }

    public void setUpPressed(boolean upPressed) {
        this.upPressed = upPressed;
    }

    public void setDownPressed(boolean downPressed) {
        this.downPressed = downPressed;
    }
}
