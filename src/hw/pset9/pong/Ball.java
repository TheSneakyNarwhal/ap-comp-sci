package hw.pset9.pong;

import processing.core.PApplet;

public class Ball {
    public static int RADIUS = 30;

    private PApplet pApplet;

    private int x;
    private int y;

    private int speedX;
    private int speedY;

    public Ball(PApplet pApplet, int x, int y) {
        this.pApplet = pApplet;
        this.x = x;
        this.y = y;
        speedX = -3;
        speedY = 2;
    }

    public void move() {
        x += speedX;
        y += speedY;
    }

    public void display() {
        pApplet.fill(255);
        pApplet.ellipse(x, y, RADIUS * 2, RADIUS * 2);
    }

    public void checkCollision(Paddle paddle1, Paddle paddle2) {
        checkWalls();
        if (checkPaddle(paddle1) || checkPaddle(paddle2)) {
            speedX *= -1;
        }
    }

    private void checkWalls() {
        if (y < RADIUS || y > pApplet.height - RADIUS) {
            speedY *= -1;
        }
    }

    private boolean checkPaddle(Paddle paddle) {
        if (y > paddle.getY() && y < paddle.getY() + Paddle.HEIGHT) {
            int minDist = Paddle.WIDTH + RADIUS;
            if (x > RADIUS && x < minDist ) {
                x = minDist;
                return true;
            }
            if (x < pApplet.width - RADIUS && x > pApplet.width - minDist) {
                x = pApplet.width - minDist;
                return true;
            }
        }
        return false;
    }
}
