package hw.pset9.pong;

import processing.core.PApplet;

public class Pong extends PApplet {
    Ball   ball;
    Paddle paddle1;
    Paddle paddle2;

    public static void main(String[] args) {
        PApplet.main(new String[]{"hw.pset9.pong.Pong"});
    }

    public void setup() {
        size(800, 500);
        ball = new Ball(this, width / 2, height / 2);
        paddle1 = new Paddle(this, 0);
        paddle2 = new Paddle(this, width - Paddle.WIDTH);
    }

    public void draw() {
        background(0);

        paddle1.move();
        paddle2.move();
        ball.move();
        ball.checkCollision(paddle1, paddle2);

        paddle1.display();
        paddle2.display();
        ball.display();
    }

    public void keyPressed() {
        if (key == CODED) {
            if (keyCode == UP) {
                paddle2.setUpPressed(true);
            } else if (keyCode == DOWN) {
                paddle2.setDownPressed(true);
            }
        } else {
            if (key == 'w' || key == 'W') {
                paddle1.setUpPressed(true);
            } else if (key == 's' || key == 'S') {
                paddle1.setDownPressed(true);
            }
        }
    }

    public void keyReleased() {
        if (keyCode == UP) {
            paddle2.setUpPressed(false);
        } else if(keyCode == DOWN) {
            paddle2.setDownPressed(false);
        } else if (key == 'w' || key == 'W'){
            paddle1.setUpPressed(false);
        } else if (key == 's' || key == 'S') {
            paddle1.setDownPressed(false);
        }
    }

}
