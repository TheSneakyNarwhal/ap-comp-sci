package hw.pset9;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class P9E {
    public static void main(String[] args) throws IOException {
        System.out.println(checkIfFileContains("sample1.txt", "Around a"));
        System.out.println(checkIfFileContains("sample1.txt", "sober"));
        System.out.println(checkIfFileContains("sample1.txt", "derrick"));

        System.out.println(checkIfFileContains("sample2.txt", "After"));
        System.out.println(checkIfFileContains("sample2.txt", "Failure"));
        System.out.println(checkIfFileContains("sample2.txt", "derrick"));
    }

    public static boolean checkIfFileContains(String path, String str) throws IOException {
        Scanner scanner = new Scanner(new File(path));
        while (scanner.hasNext()) {
            if (scanner.nextLine().contains(str)) return true;
        }
        return false;
    }
}
