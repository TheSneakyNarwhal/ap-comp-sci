package hw.pset1;

import java.util.Scanner;

public class P1C {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        println("Enter your age in years: ");

        double enteredAge = scanner.nextDouble();
        println("You are " + enteredAge + " years old");
    }

    public static void println(String string) {
        System.out.println(string);
    }

}
