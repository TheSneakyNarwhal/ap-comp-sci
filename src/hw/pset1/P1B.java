package hw.pset1;

public class P1B {

    public static void main(String[] args) {
        println("*************");
        println(" *         *");
        println("  * YIELD *");
        println("   *     *");
        println("    *   *");
        println("     * *");
        println("      *");
    }

    static void println(String string) {
        System.out.println(string);
    }

}
