package hw.pset1;

import java.util.Scanner;

public class P1F {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print("Enter your age in years: ");

        int enteredAge = scanner.nextInt();
        println("You have been alive for " + enteredAge * 365 * 24 * 60 * 60 + " seconds");

        /**
         * Using an integer would not be as accurate a double because
         * the entered age would have to be rounded
         */
    }

    public static void println(String string) {
        System.out.println(string);
    }

    public static void print(String string) {
        System.out.print(string);
    }

}
