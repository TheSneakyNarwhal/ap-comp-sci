package hw.pset1;

import java.util.Scanner;

public class P1E {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print("Enter your age in years: ");

        double enteredAge = scanner.nextDouble();
        println("You have been alive for " + enteredAge * 365 * 24 * 60 * 60 + " seconds");
    }

    public static void println(String string) {
        System.out.println(string);
    }

    public static void print(String string) {
        System.out.print(string);
    }
}
