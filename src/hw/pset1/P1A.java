package hw.pset1;

public class P1A {

    public static void main(String[] args) {
        println("Favorites:");
        println("Ice cream: Cookies & Cream");
        println("Color: Purple");
        println("Movie: Twilight");
        println("Musical Group: ABBA");
    }

    static void println(String string) {
        System.out.println(string);
    }
}