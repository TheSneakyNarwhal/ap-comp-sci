package test.test4;

public class ClassScheduleTester {

    public static void main(String[] args) {
        ClassSchedule schedule = new ClassSchedule(8);
        schedule.addCourse(1, "Calculus");
        schedule.addCourse(2, "Econ");
        schedule.addCourse(3, "MCL");
        schedule.addCourse(4, "Lunch");
        schedule.addCourse(5, "Biotech");
        schedule.addCourse(6, "AP Comp Sci");
        schedule.addCourse(7, "APEL");

        System.out.println("Lunch Period: " + schedule.findCourse("Lunch"));
        System.out.println("Government Period: " + schedule.findCourse("Government"));

        System.out.println("\nAP Courses:");
        String[] apCourses = schedule.getAPCourses();
        for (String course : apCourses) {
            System.out.println("\t" + course);
        }
        System.out.println();

        System.out.println(schedule);
    }

}
