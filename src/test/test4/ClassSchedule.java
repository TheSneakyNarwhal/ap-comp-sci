package test.test4;

import java.util.ArrayList;

// Derrick Liu, Problem B
public class ClassSchedule {
    private String[] courses;

    ClassSchedule(int numPeriods) {
        courses = new String[numPeriods];

        for (int i = 0; i < numPeriods; i++) {
            courses[i] = "";
        }
    }

    public void addCourse(int period, String courseName) {
        courses[period] = courseName;
    }

    public int findCourse(String courseName) {
        for (int i = 0; i < courses.length; i++) {
            if (courses[i].equals(courseName)) return i;
        }

        return -1;
    }

    public String[] getAPCourses() {
        ArrayList<String> apCourses = new ArrayList<>();

        for (String course : courses) {
            if (!course.isEmpty() && course.substring(0, 2).equals("AP")) {
                apCourses.add(course);
            }
        }

        String[] arr = new String[apCourses.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = apCourses.get(i);
        }

        return arr;
    }

    public String toString() {
        String string = "";

        for (int i = 0; i < courses.length; i++) {
            string += i + "\t" + courses[i] + "\n";
        }

        return string;
    }

}
