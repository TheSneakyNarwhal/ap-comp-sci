package test.test3;

// Derrick Liu, Problem A

public class LongestWord {
    public static void main(String[] args) {
        String[] myWords = {"These", "are", "some", "of", "the", "longest", "words", "I", "know", "visible", "goofy"};
        String longest = findLongestWord(myWords);
        System.out.println(longest);
    }

    public static String findLongestWord(String[] words) {
        int indexOfLongest = 0;

        for (int i = 1; i < words.length; i++) {
            if (words[indexOfLongest].length() <  words[i].length()) indexOfLongest = i;
        }

        return words[indexOfLongest];
    }
}
