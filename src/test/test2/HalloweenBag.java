package test.test2;

public class HalloweenBag {
    private int count;

    public HalloweenBag(int startingPieces) {
        count = startingPieces;
    }

    int getCount() {
        return count;
    }

    void addCandy(int newCandy) {
        count += newCandy;
    }

    void share(HalloweenBag other) {
        int otherCount = other.getCount();
        if (count > otherCount) {
            int diff = count - otherCount;
            int amtToGive = diff / 2;
            count -= amtToGive;
            other.addCandy(amtToGive);
        }
    }

}
