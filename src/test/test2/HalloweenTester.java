package test.test2;

import java.util.Scanner;

// Derrick Liu, Problem A
public class HalloweenTester {

    public static void main(String[] args) {
        HalloweenBag halloweenBag1 = new HalloweenBag(4);
        halloweenBag1.addCandy(2);

        System.out.println("Pieces of candy: " + halloweenBag1.getCount());

        HalloweenBag halloweenBag2 = new HalloweenBag(1);

        halloweenBag1.share(halloweenBag2);
        System.out.println("Pieces of candy in halloweenBag1: " + halloweenBag1.getCount());
        System.out.println("Pieces of candy in halloweenBag2: " + halloweenBag2.getCount());

        halloweenBag1.addCandy(3);

        halloweenBag2.share(halloweenBag1);
        System.out.println("Pieces of candy in halloweenBag1: " + halloweenBag1.getCount());
        System.out.println("Pieces of candy in halloweenBag2: " + halloweenBag2.getCount());

        Scanner scanner = new Scanner(System.in);
        String string1 = scanner.nextLine();
        String string2 = string1;
        System.out.println(string1 == string2);
        String string3 = scanner.nextLine();
        System.out.println(string1 == string3);
    }

}
