package test.test1;

import java.util.Random;
import java.util.Scanner;

// Derrick Liu, Problem B

public class Test1B {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number between 1 and 20: ");

        int inputNumber = scanner.nextInt();

        while (inputNumber <= 1 || inputNumber >= 20) {
            System.out.println("That is not a valid number.");
            System.out.print("Enter a number between 1 and 20: ");
            inputNumber = scanner.nextInt();
        }

        Random random = new Random();
        int sum = 0;
        for (int i = 0; i < inputNumber; i++) {
            int randomNum = random.nextInt(5) + 1;
            sum += randomNum;
            System.out.println(randomNum);
        }
        System.out.println("Sum: " + sum);
    }
}
